var webpack = require('webpack');
var path = require('path');

module.exports = {
  devtool: '#eval-source-map',

  entry: [
    './src/main'
  ],

  output: {
    path: path.join(__dirname, 'app'),
    publicPath: '/',
    filename: 'dist/bundle.js'
  },

  plugins: [
    new  webpack.optimize.OccurrenceOrderPlugin(),
    new  webpack.HotModuleReplacementPlugin(),
    new  webpack.NoEmitOnErrorsPlugin
 ],

 externals: {
  jwplayer: 'jwplayer'
},

  module: {
    loaders: [
      { 
        test: /\.json$/,
        loader: 'json-loader',
        loader: "babel-loader",
        test: /\.jsx?$/,
        exclude: /node_modules/,
        // Options to configure babel with
        query: {
          plugins: ['transform-runtime'],
          presets: ['env','react']
        }
      },
    ]
  }
};
